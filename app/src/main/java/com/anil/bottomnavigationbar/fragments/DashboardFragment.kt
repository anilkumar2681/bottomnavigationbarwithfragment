package com.anil.bottomnavigationbar.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.anil.bottomnavigationbar.R


class DashboardFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
         return inflater.inflate(R.layout.fragment_dashboard, container, false)
    }
    companion object {

        fun newInstance(): DashboardFragment {
            val fragmentDashboard = DashboardFragment()
            val args = Bundle()
            fragmentDashboard.arguments = args
            return fragmentDashboard
        }
    }
}
