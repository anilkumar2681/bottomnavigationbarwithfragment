package com.anil.bottomnavigationbar

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import com.anil.bottomnavigationbar.fragments.DashboardFragment
import com.anil.bottomnavigationbar.fragments.HomeFragment
import com.anil.bottomnavigationbar.fragments.NotificationFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    private var pageContent: FrameLayout? = null
    var navigation: BottomNavigationView? = null

    private val mOnNavigationItemSelectedListener=BottomNavigationView.OnNavigationItemSelectedListener { menuItem ->
        when(menuItem.itemId){
            R.id.navigation_home ->{
                val fragmentHome = HomeFragment.Companion.newInstance()
                addFragment(fragmentHome)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_dashboard ->{
                val fragmentDashboard=DashboardFragment.Companion.newInstance()
                addFragment(fragmentDashboard)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_notification ->{
                val fragmentNotification =NotificationFragment.Companion.newInstance()
                addFragment(fragmentNotification)
                return@OnNavigationItemSelectedListener true
            }

        }
        false
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        pageContent = findViewById(R.id.fragmentContainer)
        navigation =  findViewById(R.id.navigation)

        navigation!!.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        val default_fragment = HomeFragment.Companion.newInstance()
        addFragment(default_fragment)

    }

    /**
     * add/replace fragment in container [FrameLayout]
     */
    private fun addFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            //.setCustomAnimations(R.anim.design_bottom_sheet_slide_in, R.anim.design_bottom_sheet_slide_out)
            .replace(R.id.fragmentContainer, fragment, fragment.javaClass.simpleName)
            .commit()
    }
}
